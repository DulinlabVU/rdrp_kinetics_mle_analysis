import logging
import numpy as np

logging.basicConfig(level=logging.INFO, format="%(levelname)s - %(message)s")

class RandomDisplacementBounds(object):
    """
    Add a random dislacement of maximum size 'stepsize' to each coordinate that updates 'x'
    in place satisfying the bounds
    """
    def __init__(self, bounds, stepsize):
        self.xmin = np.array([bound[0] for bound in bounds])
        self.xmax = np.array([bound[1] for bound in bounds])
        self.stepsize = stepsize

    def __call__(self, x):
        """take a random step but ensure the new position is within the bounds """
        logging.debug("stepsize: %s", self.stepsize)
        min_step = np.maximum(self.xmin - x, -self.stepsize)
        max_step = np.minimum(self.xmax - x, self.stepsize)

        random_step = np.random.uniform(low=min_step, high=max_step, size=np.shape(x))
        xnew = x + random_step

        return xnew


class ParameterConditions(object):
    """ Class that defines conditions for the given values"""

    def __init__(self, parameter_mapping: dict, tmin: float, NN=10):
        self.NN = NN
        self.tmin = tmin
        for name, index in parameter_mapping.items():
            setattr(self, name, index)
        self.sorted_parameters = [key for key, value in sorted(parameter_mapping.items(), key=lambda item: item[1])]
        self.ii=-1

    def __call__(self, **kwargs):
        x = kwargs["x_new"]
        self.ii += 1

        # current conditions to accept solution
        conditions = {
            "condition_exp_term": 10**x[self.pkcat] < self.NN / self.tmin,
            "condition_relation_k1_kcat": self.NN * 10**x[self.Pk1] < 10**x[self.pkcat],
            "condition_k_pause_1_2": 2 * 10**x[self.Pk2] < 10**x[self.Pk1],
            "condition_PA1": 0.01 < 10**x[self.PA1] / (10**x[self.PA1] + 10**x[self.PA2]) < 0.99,
            "condition_PA2": 0.01 < 10**x[self.PA2] / (10**x[self.PA1] + 10**x[self.PA2]) < 0.99,
            "condition_pq1": 0.001 < 10**x[self.pq1] / (1 + 10**x[self.pq1] + 10**x[self.pq2]) < 0.999,
            "condition_pq2": 0.001 < 10**x[self.pq2] / (1 + 10**x[self.pq1] + 10**x[self.pq2]) < 0.999
            }

        accepted = all(conditions.values())

        if not accepted:
            logging.debug(f"{self.ii} hop, conditions not satisfied: {[key for key, value in conditions.items() if value==False]}")
            logging.debug('parameters: %s', {name: np.round(value, 2) for name, value in zip(self.sorted_parameters, x)})
            logging.debug(f"kcat: {10**x[self.pkcat]}, exp1: {10**x[self.Pk1]}, exp2: {10**x[self.Pk2]}")

        return accepted

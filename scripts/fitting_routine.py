import logging
import numpy as np
from scipy import optimize
from scipy.optimize import OptimizeResult
import scipy.special as sps
from probability_functions import gamma_density_function, exponential_distribution, exponential_density_function

logging.basicConfig(level=logging.ERROR, format="%(message)s")


# basinhopping without local optimization results in simulated annealing
def skip_local_optimization(fun, x0, args, **options):
    return OptimizeResult(x=x0, fun=fun(x0), success=True, nfev=1)


# calculate probability density function with parameter transformations that are used for the fitting
def create_probability_function(t, NN, pkcat, Pk1, Pk2, pq1, pq2, PA1, PA2):
    tmin = min(t)
    tmax = max(t)

    # naming of parameters
    scale_nucleotide_addition = 1 / (1 + 10 ** pq1 + 10 ** pq2)
    scale_pauses = 10 ** pq1 / (1 + 10 ** pq1 + 10 ** pq2)
    scale_power_law = 10 ** pq2 / (1 + 10 ** pq1 + 10 ** pq2)
    cut_off_t = (10 ** pkcat * t / NN) ** (NN - 1)
    cut_off_factor = cut_off_t / (1 + cut_off_t)

    # probability functions
    probability_function_gamma = scale_nucleotide_addition * gamma_density_function(t, 10 ** pkcat, NN)
    probability_function_pauses = \
        cut_off_factor * scale_pauses * (
                    10 ** PA1 / (10 ** PA1 + 10 ** PA2) * exponential_density_function(t - NN / 10 ** pkcat,
                                                                                       10 ** Pk1) +
                    (10 ** PA2 / (10 ** PA1 + 10 ** PA2) * exponential_density_function(t - NN / 10 ** pkcat,
                                                                                        10 ** Pk2)))

    probability_function_backtrack = cut_off_factor * scale_power_law * (
            np.sqrt(1 + NN / 10 ** pkcat) / 2 * (1 + t) ** (-3 / 2))

    probability_function = probability_function_gamma + probability_function_pauses + probability_function_backtrack

    # normalizing constants
    normalization_term_gamma = scale_nucleotide_addition * (
            -sps.gammaincc(NN, 10 ** pkcat * tmax) + sps.gammaincc(NN, 10 ** pkcat * tmin))
    normalization_term_pauses = \
        scale_pauses * (
                    10 ** PA1 / (10 ** PA1 + 10 ** PA2) * exponential_distribution(tmax - NN / 10 ** pkcat, 10 ** Pk1) +
                    10 ** PA2 / (10 ** PA1 + 10 ** PA2) * exponential_distribution(tmax - NN / 10 ** pkcat, 10 ** Pk2))

    normalization_term_backtrack = scale_power_law * (1 - np.sqrt(1 + NN / 10 ** pkcat) / (np.sqrt(1 + tmax)))

    normalization_terms = normalization_term_gamma + normalization_term_pauses + normalization_term_backtrack

    # calculate probability density function
    probability_density_function = probability_function / normalization_terms

    return probability_density_function, probability_function, normalization_terms


def fit_MLE_simulated_annealing(data, parameter_guesses, acceptance_conditions, n_iter,
                                temperature, takestep, callback_basinhopping, update_stepsize_interval, NN=10):
    def MLE(parameters):
        pkcat, Pk1, Pk2, pq1, pq2, PA1, PA2 = parameters
        pdf, pf, norms = create_probability_function(t=data, NN=NN,
                                                     pkcat=pkcat, Pk1=Pk1, Pk2=Pk2, pq1=pq1, pq2=pq2, PA1=PA1, PA2=PA2)
        negative_log_likelihood = -np.sum(np.log(pdf))
        return negative_log_likelihood

    # use simulated annealing for optimization
    minimizer_kwargs = dict(method=skip_local_optimization)
    result = optimize.basinhopping(func=MLE, x0=parameter_guesses, niter=n_iter, T=temperature,
                                   take_step=takestep, minimizer_kwargs=minimizer_kwargs,
                                   accept_test=acceptance_conditions, callback=callback_basinhopping,
                                   interval=update_stepsize_interval)

    return result


# search the local space for a better solution using constrained optimization
def fit_MLE_local_optimization(data, parameter_guesses, parameter_bounds, method="L-BFGS-B", NN=10):
    def MLE(parameters):
        pkcat, Pk1, Pk2, pq1, pq2, PA1, PA2 = parameters
        pdf, pf, norms = create_probability_function(t=data, NN=NN,
                                                     pkcat=pkcat, Pk1=Pk1, Pk2=Pk2, pq1=pq1, pq2=pq2, PA1=PA1, PA2=PA2)
        negative_log_likelihood = -np.sum(np.log(pdf))
        return negative_log_likelihood

    result = optimize.minimize(MLE, x0=parameter_guesses, bounds=parameter_bounds, method=method)
    return result.x, result.fun


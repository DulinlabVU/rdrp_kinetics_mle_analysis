import logging
import numpy as np
import matplotlib.pyplot as plt

NUMBER_BINS = 35
np.random_seed = 89


def generate_exponential_data_with_cut_off(size: int, scale: float, cut_off):
    exponential_data = np.random.exponential(scale, 5*size)
    if cut_off:
        exponential_data = np.random.choice(exponential_data[np.where(exponential_data > cut_off)], size)
    return exponential_data


def generate_shifted_exponential_data(size: int, scale: float, cut_off):
    exponential_data = np.random.exponential(scale, size)
    if cut_off:
        exponential_data = exponential_data + cut_off
    return exponential_data


def simulate_t_gamma_two_exp_powerlaw(size: int, probability_exp: list, backtrack_prob: float,
                                      kcat: float, NN: int, k_exp: list):
    gamma_scale = 1 / kcat
    cut_off_exp = NN / kcat
    gamma_shape = NN
    probability_gamma = 1 - sum(probability_exp) - backtrack_prob
    logging.info("prob gamma: %s, prob pauses: %s",probability_gamma, probability_exp)
    logging.info("nr datapoints total: %s, pause 1: %s, pause 2: %s, backtrack: %s",
                 size, int(size*probability_exp[0]), int(size*probability_exp[1]), int(size*backtrack_prob))

    # generating t's from gamma distribution
    gamma_data = np.random.gamma(gamma_shape, gamma_scale, int(size*probability_gamma))

    # generating t's from exponential distribution
    exp_data = np.array([])
    for prob, k_value in zip(probability_exp, k_exp):
        new_data = generate_shifted_exponential_data(int(size*prob), 1/k_value, cut_off_exp)
        exp_data = np.concatenate((exp_data, new_data))

    # generating t's from powerlaw distribution
    powerlaw_data = (1+NN/kcat)*(1-np.random.uniform(size=int(size*backtrack_prob)))**(-1/(3/2-1))

    # combine the data into one dataset
    combined_data = np.concatenate((gamma_data, exp_data, powerlaw_data))
    return np.sort(combined_data)


def plot_binned_data(ydata, title='dwelltime log plot', number_of_bins=NUMBER_BINS):
    # creating the normalized log binned histogram data
    bins = 10 ** np.linspace(np.log10(np.min(ydata)), np.log10(np.max(ydata)), number_of_bins)
    counts, bin_edges = np.histogram(ydata, bins=bins)
    bin_centres = (bin_edges[:-1] + bin_edges[1:]) / 2
    bin_width = (bin_edges[1:] - bin_edges[:-1])
    # the integral of np.trapz(bin_centres, normalized_counts) = 1
    # so this data represents the probability density function of the distribution function
    normalized_counts = counts / bin_width / np.sum(counts)
    plt.figure()
    # plot actual data
    plt.plot(bin_centres, normalized_counts, 'k+', label="normalized binned data")
    plt.xlabel("Dwell Times (s)")
    plt.xlim([min(ydata), max(ydata)])
    plt.ylabel("Probability")
    plt.xscale('log')
    plt.yscale('log')
    plt.title(title)


def plot_binned_data_LnF(ydata, number_of_bins=NUMBER_BINS):
    bins = 10 ** np.linspace(np.log10(np.min(ydata)), np.log10(np.max(ydata)), number_of_bins)
    counts, bin_edges = np.histogram(ydata, bins=bins)
    # select only bins where the count is more than 0, merge the other bins
    bins_count_positive = []
    for ii, bin in zip(range(1, len(bins)), bins):
        if counts[ii-1] > 0:
            bins_count_positive.append(bin)
    # am I missing the last edge?
    counts, bin_edges = np.histogram(ydata, bins=bins_count_positive)
    bin_width = (bin_edges[1:] - bin_edges[:-1])
    # or np.sqrt?
    bin_centres_sqrt = np.sqrt((bin_edges[:-1] * bin_edges[1:]))
    bin_centres = (bin_edges[:-1] + bin_edges[1:]) / 2
    normalized_counts = counts / bin_width / np.sum(counts)
    plt.plot(bin_centres_sqrt, normalized_counts, 'bo', label="sqrt centered binned data")
    plt.xlabel("Dwell Times (s)")
    plt.xlim([min(ydata), max(ydata)])
    plt.ylabel("Probability")
    plt.xscale('log')
    plt.yscale('log')
    plt.legend()
    return bin_centres, bin_centres_sqrt, normalized_counts


def get_binned_data(ydata, number_of_bins=35):
    # creating the normalized log binned histogram data
    bins = 10 ** np.linspace(np.log10(np.min(ydata)), np.log10(np.max(ydata)), number_of_bins)
    counts, bin_edges = np.histogram(ydata, bins=bins)
    bin_centres = (bin_edges[:-1] + bin_edges[1:]) / 2
    bin_width = (bin_edges[1:] - bin_edges[:-1])
    normalized_counts = counts / bin_width / np.sum(counts)
    return bin_centres, normalized_counts


def get_sigma_cutoff_value(data: np.array, number_of_bins: int, sigma_cut: float, NN: int):
    bin_centres, normalized_counts = get_binned_data(data, number_of_bins)
    peak_index = np.argmax(normalized_counts)
    peak_dwelltime = bin_centres[peak_index]
    sigma_cut = peak_dwelltime*(1-sigma_cut/np.sqrt(NN))
    return sigma_cut



import configparser

import logging
import numpy as np
from classes_for_fit_routines import RandomDisplacementBounds, ParameterConditions
from probability_functions import create_probability_function, convert_fitted_parameters
import matplotlib.pyplot as plt
from simulate_and_plot_data import plot_binned_data
from fitting_routine import fit_MLE_simulated_annealing, fit_MLE_local_optimization

logging.basicConfig(level=logging.DEBUG, format="%(message)s")


class Model:

    def __init__(self):
        self.data = None
        self.fit_parameters_dict = {}
        self.read_config()
        self.initialize_fit_parameters()

    def read_config(self):
        self.config = configparser.RawConfigParser()
        self.config.optionxform = str
        self.config.read(r'config.ini')

    def load_dwelltime_data(self, dwelltimes):
        self.data = np.sort(dwelltimes)

    def initialize_fit_parameters(self):
        # set order of fit parameters
        self.parameter_mapping = {name: int(index) for name, index in self.config["ParameterMapping"].items()}
        self.sorted_parameters = [key for key, value in
                                  sorted(self.parameter_mapping.items(), key=lambda item: item[1])]
        self.set_guesses({})
        self.set_parameter_bounds({})
        self.set_simulated_annealing_parameters({})

    def set_simulated_annealing_parameters(self, parameters):
        mapping = {'stepsize': float, 'temperature': float, 'iterations': int, 'interval': int}
        if not parameters:
            parameters = self.config['SimulatedAnnealing']
        for name, value in parameters.items():
            setattr(self, name, mapping[name](value))

    def set_guesses(self, guesses):
        if not guesses:
            # set default guesses for fit parameters
            guesses = self.config["DefaultFitParameters"]
        for name, value in guesses.items():
            setattr(self, name, float(value))
            self.fit_parameters_dict[name] = float(value)
        # store guesses for fit parameters in the right order for the MLE input
        self.guesses = [getattr(self, name) for name in self.sorted_parameters]

    def set_parameter_bounds(self, bounds):
        if not bounds:
            # set default bounds for fit parameters
            bounds = {key: eval(value) for key, value in self.config["DefaultFitBounds"].items()}
        self.bounds = [(bounds[param + '_min'], bounds[param + '_max']) for param in
                       self.sorted_parameters]

    def get_converted_fit_paramaters(self):
        result = convert_fitted_parameters(self.fit_parameters_dict, self.NN)
        self.converted_parameters = result
        return result

    def update_fit_parameters_dict(self):
        for fit_parameter in self.fit_parameters_dict:
            self.fit_parameters_dict[fit_parameter] = getattr(self, fit_parameter)
        # recalculate the probability density function based on the new fit parameters
        self.calculate_pdf_and_likelihood()
        # set the guesses to the fitted parameters
        self.set_guesses(self.fit_parameters_dict)
        logging.info("fitted parameters are set to {}".format(self.fit_parameters_dict))
        logging.info(
            "converted parameters (one-nucleotide probabilities): {}".format(self.get_converted_fit_paramaters()))

    def fit_MLE_simulated_annealing(self):
        self.calculate_pdf_and_likelihood()
        self.bound_step = RandomDisplacementBounds(bounds=self.bounds, stepsize=self.stepsize)
        acceptance_conditions = ParameterConditions(parameter_mapping=self.parameter_mapping, tmin=min(self.data),
                                                    NN=self.NN)

        # initialize list for accepted points by the simulated annealing MLE fit
        x_accepted = [self.guesses]
        f_accepted = [self.log_likelihood]

        def store_accepted_hops(x, f, accepted):
            if accepted:
                x_accepted.append(x)
                f_accepted.append(f)
                logging.debug("callback of basinhopping at minimum %.2f accepted %d" % (f, int(accepted)))

        logging.info("start simulated annealing algorithm for maximum likelihood estimation ...")
        logging.info("with parameter guesses: {}".format({name: value for name, value
                                                          in zip(self.sorted_parameters, self.guesses)}))

        result = fit_MLE_simulated_annealing(data=self.data, parameter_guesses=self.guesses,
                                             acceptance_conditions=acceptance_conditions,
                                             n_iter=self.iterations, temperature=self.temperature,
                                             takestep=self.bound_step,
                                             callback_basinhopping=store_accepted_hops,
                                             update_stepsize_interval=self.interval, NN=self.NN)

        logging.info("simulated annealing is done...")
        self.set_fitted_parameters(result.x)
        self.x_accepted, self.f_accepted = x_accepted, f_accepted
        return result.x

    def set_fitted_parameters(self, x_result):
        for name, value in zip(self.sorted_parameters, x_result):
            setattr(self, name, value)
        self.update_fit_parameters_dict()

    def fit_MLE_local_optimization(self):
        local_x, local_fun = fit_MLE_local_optimization(data=self.data, parameter_guesses=self.guesses,
                                                        parameter_bounds=self.bounds, method="L-BFGS-B", NN=self.NN)
        acceptance_conditions = ParameterConditions(parameter_mapping=self.parameter_mapping, tmin=min(self.data),
                                                    NN=self.NN)
        accept_local_x = acceptance_conditions(x_new=local_x)
        if accept_local_x and local_fun < self.log_likelihood:
            logging.info("local optimization found better solution")
            self.log_likelihood = local_fun
            self.set_fitted_parameters(local_x)
            return local_x
        else:
            logging.info("local optimization did not find better solution")
            return None

    def calculate_pdf_and_likelihood(self):
        self.pdf, _, _ = create_probability_function(t=self.data, **self.fit_parameters_dict)
        self.pdf_integral = np.trapz(self.pdf, self.data)
        self.log_likelihood = -np.sum(np.log(self.pdf))
        logging.info("log likelihood: {}".format(self.log_likelihood))

    def plot_data(self):
        plot_binned_data(self.data)
        plt.show()

    def plot_fit(self):
        plot_binned_data(self.data)
        plt.scatter(self.data, self.pdf, label="fitted pdf", s=4)
        plt.xlim([min(self.data), max(self.data)])
        plt.legend()
        plt.show()

    def plot_simulated_annealing_hops(self):
        plt.figure()
        plt.plot(self.f_accepted, 'o-')
        plt.ylabel("negative log likelihood")
        plt.xlabel("accepted hop")
        plt.title("simulated annealing")
        plt.show()

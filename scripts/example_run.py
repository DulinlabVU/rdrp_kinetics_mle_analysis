from model import Model
import pandas as pd
from simulate_and_plot_data import simulate_t_gamma_two_exp_powerlaw
from probability_functions import prob_to_dwelltime_prob

# data example with simulated data
# notice that one might have to change the initial guesses
# or the simulated annealing parameters (changing the scanned parameters space)
# in the config.ini file or programmatically to get the best results

gamma_rate = 10
exp1_rate = 0.5
exp2_rate = 0.1
exp1_prob = 0.05
exp2_prob = 0.003
powerlaw_prob = 0.0005
dwelltime_prob_na, dwelltime_prob_pauses = prob_to_dwelltime_prob(prob_pauses=[0.05, 0.003, 0.0005], N_dw=10)
data = simulate_t_gamma_two_exp_powerlaw(size=10000, probability_exp=dwelltime_prob_pauses[0:2],
                                         backtrack_prob=dwelltime_prob_pauses[2], kcat=10, NN=10, k_exp=[0.5, 0.1])

fits = []

for ii in range(5):
    model_data = Model()
    model_data.load_dwelltime_data(data)

    # first fit with the simulated annealing algorithm, than perform local optimization of the best solution
    model_data.fit_MLE_simulated_annealing()
    model_data.fit_MLE_local_optimization()

    fitted_parameters = model_data.get_converted_fit_paramaters()
    fits.append(fitted_parameters)

dataframe_fits = pd.DataFrame(fits)
print(dataframe_fits.mean())
model_data.plot_fit()
model_data.plot_simulated_annealing_hops()




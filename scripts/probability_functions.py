import numpy as np
import scipy.special as sps


def exponential_density_function(x, lamda):
    return lamda * np.exp(-lamda * x)
    # equals scipy.stats.expon.pdf(x, scale=1/lamda)


def exponential_distribution(x, lamda):
    return 1 - np.exp(-lamda * x)
    # equals scipy.stats.expon.cdf(x, scale=1/lamda)


def gamma_density_function(x, lamda, shape):
    return exponential_density_function(x, lamda) * (lamda * x) ** (shape - 1) / sps.gamma(shape)


def create_probability_function(t, NN, pkcat, Pk1, Pk2, pq1, pq2, PA1, PA2):
    tmin = min(t)
    tmax = max(t)

    # naming of parameters
    scale_nucleotide_addition = 1 / (1 + 10**pq1 + 10**pq2)
    scale_pauses = 10**pq1 / (1 + 10**pq1 + 10**pq2)
    scale_power_law = 10**pq2 / (1 + 10**pq1 + 10**pq2)
    cut_off_t = (10**pkcat * t / NN) ** (NN - 1)
    cut_off_factor = cut_off_t / (1 + cut_off_t)

    # probability functions
    probability_function_gamma = scale_nucleotide_addition * gamma_density_function(t, 10**pkcat, NN)
    probability_function_pauses = \
        cut_off_factor * scale_pauses * (10**PA1/(10**PA1+10**PA2) * exponential_density_function(t - NN / 10**pkcat, 10**Pk1) +
                                         (10**PA2/(10**PA1+10**PA2) * exponential_density_function(t - NN / 10**pkcat, 10**Pk2)))

    probability_function_backtrack = cut_off_factor * scale_power_law * (
            np.sqrt(1 + NN / 10**pkcat) / 2 * (1 + t) ** (-3 / 2))

    probability_function = probability_function_gamma + probability_function_pauses + probability_function_backtrack

    # normalizing constants
    normalization_term_gamma = scale_nucleotide_addition * (
            -sps.gammaincc(NN, 10**pkcat * tmax) + sps.gammaincc(NN, 10**pkcat * tmin))
    normalization_term_pauses = \
        scale_pauses * (10**PA1/(10**PA1+10**PA2) * exponential_distribution(tmax - NN / 10**pkcat, 10**Pk1) +
                        10**PA2/(10**PA1+10**PA2) * exponential_distribution(tmax - NN / 10**pkcat, 10**Pk2))

    normalization_term_backtrack = scale_power_law * (1 - np.sqrt(1 + NN / 10**pkcat) / (np.sqrt(1 + tmax)))

    normalization_terms = normalization_term_gamma + normalization_term_pauses + normalization_term_backtrack

    # calculate probability density function
    probability_density_function = probability_function / normalization_terms

    return probability_density_function, probability_function, normalization_terms


def get_pq1_pq2_pA2(q1, q2, A2, PA1=0):
    PA2 = np.log(-A2 * np.exp(PA1 * np.log(10)) / (A2 - 1)) / np.log(10)
    pq1 = np.log10(q1)
    pq2 = np.log10(q2)
    return pq1, pq2, PA2


# from fit parameters
def get_dwelltime_probabilities(pq1, pq2, PA1, PA2):
    sum_params = (1+10**pq1+10**pq2)
    A1 = 10**PA1 / (10**PA1 + 10**PA2)
    A2 = 10**PA2 / (10**PA1 + 10**PA2)
    ppna = 1 / sum_params
    pp1 = 10**pq1 * A1/sum_params
    pp2 = 10**pq1 * A2/sum_params
    ppb = 10**pq2/sum_params
    return ppna, pp1, pp2, ppb


def get_q1_q2_A1_A2_from_dwelltime_probabilities(pp1, pp2, ppb):
    q2 = -ppb/(pp1 + pp2 + ppb - 1)
    q1 = -(pp1 + pp2)/(pp1 + pp2 + ppb - 1)
    try:
        A1 = pp1 / (pp1+pp2)
    except ZeroDivisionError:
        A1 = 0
    try:
        A2 = pp2 / (pp1+pp2)
    except ZeroDivisionError:
        A2=0
    return q1, q2, A1, A2


# probability pauses for 1-nt converted to probability dwell time window contains this pause for simulating data
def prob_to_dwelltime_prob(prob_pauses: list, N_dw: int):
    prob_nucleotide_addition = 1-sum(prob_pauses)
    dwelltime_prob_na = prob_nucleotide_addition**N_dw
    dwelltime_prob_pauses = []
    for n, pause in enumerate(prob_pauses):
        no_pauses_longer_than_n = sum([prob_nucleotide_addition]+[prob_pauses[ii] for ii in range(n+1)])**N_dw
        no_pauses_longer_than_n_1 = sum([prob_nucleotide_addition]+[prob_pauses[ii] for ii in range(n)])**N_dw
        dwelltime_prob_pause = no_pauses_longer_than_n - no_pauses_longer_than_n_1
        dwelltime_prob_pauses.append(dwelltime_prob_pause)
    return dwelltime_prob_na, dwelltime_prob_pauses


def prob_dwelltime_to_1nt_prob(prob_pauses: list, N_dw: int):
    prob_nucleotide_addition = prob_pauses[0]**(1/N_dw)
    cycle_prob_pauses = [prob_nucleotide_addition]
    for n in range(1, len(prob_pauses)):
        no_pauses_longer_than_n = sum([prob_pauses[ii] for ii in range(n+1)])**(1/N_dw)
        no_pauses_longer_than_n_1 = sum([prob_pauses[ii] for ii in range(n)])**(1/N_dw)
        cycle_prob_pause = no_pauses_longer_than_n - no_pauses_longer_than_n_1
        cycle_prob_pauses.append(cycle_prob_pause)
    return cycle_prob_pauses


# convert back to relevant parameters
def convert_fitted_parameters(fits, N_dw):
    param_dict = {}
    param_dict["gamma_rate"] = np.round(10 ** fits.get("pkcat"), 2)
    param_dict["exp1_rate"] = np.round(10 ** fits.get("Pk1"), 4)
    param_dict["exp2_rate"] = np.round(10 ** fits.get("Pk2"), 4)
    ppna, pp1, pp2, ppb = \
        get_dwelltime_probabilities(fits.get("pq1"), fits.get("pq2"), fits.get("PA1"), fits.get("PA2"))
    ppna_1nt, pp1_1nt, pp2_1nt, ppb_1nt = prob_dwelltime_to_1nt_prob(prob_pauses=[ppna, pp1, pp2, ppb], N_dw=N_dw)
    param_dict["gamma_prob"] = np.round(ppna_1nt, 3)
    param_dict["exp1_prob"] = np.round(pp1_1nt, 5)
    param_dict["exp2_prob"] = np.round(pp2_1nt, 5)
    param_dict["powerlaw_prob"] = np.round(ppb_1nt, 5)
    return param_dict


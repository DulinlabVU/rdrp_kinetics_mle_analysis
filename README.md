# Fitting dwelltime data

This repository aims to assist in fitting dwelltime data obtained in RNA-dependent RNA polymerase (RdRP) experiments.
It will be updated in the future with more functionalities and documentation.

### Fitting of dwelltime data with maximum likelihood estimation of parameters ###
- To fit the data, a [simulated annealing algorithm](https://www.youtube.com/watch?v=XNMGq5Jjs5w) is used to find the MLE of the parameters by minimizing the negative log likelihood value of the pdf. This algorithm is implemented using the [scipy basinhopping method](https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.basinhopping.html). Read the documentation and source code to get a better understanding of the parameters. 
- Notes on parameters
    - *stepsize*: measure for the random displacement of the fit parameters (notice that the stepsize for all the parameters is equal here (!)) 
    - The fit parameters, are transformed as paremeter=10**fit_parameter, such that these equal sized steps make more sense. 
    - The *temperature* is a measure for the acceptance of the new point in parameter space based on the difference in the -log likelihood value that will be accepted. Any hop that has a lower log likelihood will always be accepted, if it has a higher value, the temperature (and a chance factor) decides if the hop is accepted. Increasing the temperature will thus explore a larger parameter space.
    - the *iterations* set how many times a new point in parameter space is chosen (independent of whether it is accepted or not). It is the main factor for the computation time.
    - the *interval update* sets after how many iterations the stepsize is decreased -> over time the stepsize has to decrease to converge to the minimum in a local minimum well. If you lower the iterations you might want to lower this value as well.

See for an example run that loads data, performs simulated annealing and local optimization and plots the resulting fitted probability density function in scripts/example_run.py

![Example result](dwelltime_logplot.png)
